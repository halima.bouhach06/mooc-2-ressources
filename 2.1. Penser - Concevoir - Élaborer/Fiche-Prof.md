**Thématique** : Algorithmique et programmation.


**Notions liées** : Instruction, Boucle, structure répetition et structure conditionnelle.


**Résumé de l’activité** : Les élèves vont calculer en utilisant des instructions sous le langage python.


**Objectifs** : Maitriser la notion des instructions.


**Auteur** : Halima Bouhach.


**Durée de l’activité** : 2H


**Forme de participation** : En groupe.


**Matériel nécessaire** : Tableau, Datashow, Papier.


**Fiche élève cours** : _________


**Fiche élève activité** : Disponible https://www.toutatice.fr/toutatice-portail-cms-nuxeo/binary/Fiche_%C3%A9l%C3%A8ve_Les+viennoiseries.pdf?type=ATTACHED_FILE&path=%2Fespace-educ%2Fpole-sciences%2Fmathemathiques%2Faam08-09%2Ftravaux-traam-2017-2018%2Fexperimentations%2Fles-viennoiseries%2Fles-viennoiseries&portalName=default&index=0&t=1647852161
